FROM python:3.6-alpine

RUN mkdir -p /opt/backend-for-frontend
WORKDIR /opt/backend-for-frontend

COPY requirements.txt /opt/backend-for-frontend/requirements.txt
RUN pip install -r requirements.txt

ADD . .

EXPOSE 9000

# TODO: use gunicorn in production
CMD python app.py
