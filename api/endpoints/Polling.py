from flask.views import MethodView

from ..response import response_ok, response_error

from uuid import uuid4
from random import randint

class Polling(MethodView):

    def get(self, polling_id: str):
        if polling_id is None:
            return response_ok({
                "pollingId": uuid4()
            })
        else:
            return response_ok({
                "pollingId": polling_id,
                "progress":  randint(1, 100)
            })
