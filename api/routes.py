from flask import Blueprint, request

from .endpoints.Pages import Pages
from .endpoints.Categories import Categories
from .endpoints.Courses import Courses
from .endpoints.Polling import Polling

from .response import response_ok

api = Blueprint("api", __name__)

pages_view = Pages.as_view("pages")
api.add_url_rule(
    "/pages/<path:title>",
    view_func=pages_view,
    methods=["GET"]
)

categories_view = Categories.as_view("categories")
api.add_url_rule(
    "/categories/",
    defaults={"category_name": None},
    view_func=categories_view,
    methods=["GET"]
)
api.add_url_rule(
    "/categories/<category_name>",
    view_func=categories_view,
    methods=["GET"]
)

courses_vies = Courses.as_view("courses")
api.add_url_rule(
    "/courses/<course_name>",
    view_func=courses_vies,
    methods=["GET"]
)


polling_view = Polling.as_view("polling")
api.add_url_rule(
    "/polling/",
    defaults={"polling_id": None},
    view_func=polling_view,
    methods=["GET"]
)
api.add_url_rule(
    "/polling/<polling_id>",
    view_func=polling_view,
    methods=["GET"]
)


@api.route("/auth/login", methods=["GET", "POST"])
def auth_login():
    headers = {
        "Authorization": "thisisatoken"
    }
    return response_ok("ok", headers)

@api.route("/auth/user", methods=["GET", "POST"])
def auth_user():
    headers = {
        "Authorization": "thisisatoken"
    }
    return response_ok({ "name": "crisbal" }, headers)


@api.route("/auth/refresh", methods=["GET", "POST"])
def auth_refresh():
    headers = {
        "Authorization": "Bearer thisisatoken"
    }
    return response_ok("ok", headers)
