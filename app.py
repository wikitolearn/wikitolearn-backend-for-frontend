from flask import Flask
from flask_cors import CORS, cross_origin

from api import api

from api.cache import cache

app = Flask(__name__)

cache.init_app(app)
CORS(app, resources={r"/api/*": {"origins": "*"}})

app.register_blueprint(api, url_prefix='/api/v1')

app.run(debug=True, host="0.0.0.0", port=9000)
